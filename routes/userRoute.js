const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

router.get('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.getAllUser().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You are not authorized to do this`)
	}

})

router.get('/:id', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.findUser(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You are not authorized to do this`)
	}
	

})

router.get('/details/:id', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	userController.getDetails(data, req.params).then( resultFromController => res.send(resultFromController))

})

router.post('/register', (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));


})

router.post('/login', (req, res) => {


	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));


})


router.patch('/giveAdminAccess/:id', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.giveAccess(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Your are not authorized to do this")
	}

})

router.patch('/removeAdminAccess/:id', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.removeAccess(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Your are not authorized to do this")
	}

})









module.exports = router;
