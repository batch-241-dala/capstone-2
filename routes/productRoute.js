const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


router.post('/create', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("User not authorized to create product.")
	}

})


router.get('/activeProducts', auth.verify, (req, res) => {

	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));

})

router.get('/allProducts', auth.verify, (req,res)=> {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.getAllProducts().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User not authorized to update product")
	}

})

router.get('/:id', auth.verify, (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})


router.put('/:id', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User not authorized to update product")
	}
})

router.patch('/archive/:id', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.archiveProduct(req.params.id).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User not authorized to update product")
	}
})








module.exports = router;