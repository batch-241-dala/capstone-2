const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');


router.post('/create/:userId', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	if(data.isAdmin == false){
	orderController.createOrder(data, req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Only users can create order")
	}


})

router.get('/:id', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	orderController.getUserOrder(data, req.params).then(resultFromController => res.send(resultFromController));

})

router.patch('/addToCart/addProduct/:orderId', auth.verify, (req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	orderController.addProduct(data, req.params).then(resultFromController => res.send(resultFromController));

})

router.delete('/removeOrder/:orderId', auth.verify, (req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	orderController.removeOrder(data, req.params).then(resultFromController => res.send(resultFromController));

})

router.get('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Your are not authorized to do this")
	}

})







module.exports = router;