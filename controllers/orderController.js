const Product = require('../models/Products');
const User = require('../models/Users');
const Order = require('../models/Orders');
const auth = require('../auth');

module.exports.createOrder = (data, reqParams) => {

	return User.findById(reqParams.userId).then(user => {

		if(data.userId !== reqParams.userId){
			return `User Id does not match`
		} else {
			return Product.findById(data.productId).then(product => {
				let newOrder = new Order({

					userId: data.userId,
					products: [
					{
						productId: data.productId,
						quantity: data.quantity
					}
					],
					totalAmount: product.price * data.quantity

				})

				return newOrder.save().then((order, error) => {
					if(error){
						return false
					} else {
						return `Order successfully created`
					}
				})
			})
		}

	})
	

}


module.exports.getUserOrder = (data, reqParams) => {

	return User.findById(data.userId).then(user => {
		return Order.find({userId: reqParams.id}).then(order => {
			if(data.userId !== reqParams.id){
				return `User Id does not match`
			}else{
				return order
			}
		})
	})
	
}

module.exports.addProduct = (data, reqParams) => {

		let addedOrder = {

			productId: data.productId,
			quantity: data.quantity

		}

		return Order.findByIdAndUpdate(reqParams.orderId, addedOrder).then((order, error) => {
				
				if(data.userId !== order.userId){
					// check order id if existing on current user logged on
					return `Order Id does not found`
				} else {

					return Product.findById(data.productId).then(product => {

						order.products.push(addedOrder)
						order.totalAmount = order.totalAmount + (product.price * data.quantity)

						return order.save().then((result, error) => {
							if(error){
								return false
							} else {
								return `Order successfully added to cart`
							}
						})
					})	
					
				}	
		})	
}

module.exports.removeOrder = (data, reqParams) => {

		return Order.findByIdAndRemove(reqParams.orderId).then((order, error) => {
				
				if(data.userId !== order.userId){
					// check order id if existing on current user logged on
					return `Order Id does not found`
				} else {

					return `Order ${order.id} has been removed`
					
				}	
		})	
}

module.exports.getAllOrders = () => {

		return Order.find({}).then((order,error) => {
			if(error){
				return false
			} else {
				return order
			}
			
		})

}