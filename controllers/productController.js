const Product = require('../models/Products');
const User = require('../models/Users');
const Order = require('../models/Orders');
const auth = require('../auth');

module.exports.createProduct = (reqBody) => {

	return Product.find({productName: reqBody.productName}).then(result => {

		if (result.length > 0) {
			return "This product already exist."
		} else {
			let newProduct = new Product({

				productName: reqBody.productName,
				description: reqBody.description,
				price: reqBody.price

			})

			return newProduct.save().then((product, error) => {

				if(error) {
					return false
				} else {
					return "Successfully created."
				}

			})

		}

	})

}

module.exports.getActiveProducts = () => {
	
	return Product.find({isActive: true}).then( result => {

		let data = [];

		if(result.length > 0) {
			data.push(result)
			return data
		} else {
			return false
		}


	})

}

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.id).then(result => {

		if(result == null) {
			return "No such product exist"
		} else {
			return result
		}

	})

}

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
			productName: reqBody.productName,
			description: reqBody.description,
			price: reqBody.price
		}
	return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((result, error) =>{
		if(error) {
			console.log(error)
			return false
		} else {

			return `Product ${reqBody.productName} details updated`
		}

	})

}

module.exports.archiveProduct = (productId) => {

	return Product.findById(productId).then((result, error)=> {

		if(error){
			console.log(error)
			return false
		} else {

			result.isActive = false

			return result.save().then((res, err) => {
				if(err){
					console.log(err)
				} else {
					return `Product ${result.productName} has been archived.`
				}
			})

		}

	})

}

module.exports.getAllProducts = () => {

	return Product.find({}).then((products, error) => {

		if(error){
			return false
		} else {
			return products
		}

	})

}