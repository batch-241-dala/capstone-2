const User = require('../models/Users');
const Product = require('../models/Products');
const Order = require('../models/Orders');
const bcryptjs = require('bcryptjs');
const auth = require('../auth');

module.exports.registerUser = (reqBody) => {

	return User.find({email: reqBody.email}).then( result => {

		if(result.length > 0) {
			return "Email already exist. Registration Failed!"
		} else {

			let newUser = new User ({

				name: reqBody.name,
				email: reqBody.email,
				password: bcryptjs.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {

				if(error) {
					return false
				} else {

					return "Registration Successful"

				}

			})


		}

	})

}

module.exports.getAllUser = () => {

	return User.find({}).then((user, error) => {

		if(error){
			return false
		} else {
			
			return user
		}

	})

}

module.exports.findUser = (reqParams) => {

	return User.findById(reqParams.id).then((result, error) => {
		
		if(error){
			return false
		} else {

			result.password = ""
			return result
		}

	})

}

module.exports.getDetails = (data, reqParams) => {

	return User.findById(data.userId).then((result, error) => {

		if(data.userId !== reqParams.id){
			return `User ID does not match`
		} else {
			return `Here are your details:
			${result}`
		}

	})

}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {

			return `Login failed user does not exist`
		} else {

			const verifyPassword = bcryptjs.compareSync(reqBody.password, result.password);

			if(verifyPassword){
				return`Login success. Welcome ${result.name} here's your access: ${auth.createAccessToken(result)}`
			} else {
				return "Login failed. Password incorrect!"
			}
		}

	})


}




module.exports.giveAccess = (reqParams) => {

	return User.findById(reqParams.id).then((result, error) => {

		if(error){
			return false
		} else {

			result.isAdmin = true
			return result.save().then((res, err) => {
				if(err){
					console.log(err)
				} else {
					return `User ${result.name} has now admin access`
				}
			})
		}

	})

}


module.exports.removeAccess = (reqParams) => {

	return User.findById(reqParams.id).then((result, error) => {

		if(error){
			return false
		} else {

			result.isAdmin = false
			return result.save().then((res, err) => {
				if(err){
					console.log(err)
				} else {
					return `User ${result.name} admin access removed`
				}
			})
		}

	})

}