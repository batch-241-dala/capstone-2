const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({

	productName: {
		type: String,
		required: [true, "Product name must be filled"]
	},

	description: {
		type: String,
		required: [true, "Description must be filled"]
	},

	price: {
		type: Number,
		required: [true, "Price must be filled"]
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	isActive: {
		type: Boolean,
		default: true
	}

	// orders: [
	// 	{
	// 		orderId: {
	// 			type: String,
	// 			required: [true, "Order ID is required"]
	// 		}
	// 	}
	// ]


})




module.exports = mongoose.model("Product", productSchema);