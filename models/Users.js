const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name must be filled"]
	},

	email: {
		type: String,
		required: [true, "Email must be filled"]
	},

	password: {
		type: String,
		required: [true, "Password must be filled"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	
	}

	// orders: [
	// 	{
	// 		products: [
	// 			{
	// 				productId: {
	// 					type: String,
	// 					required: [true, "Product Id is required"]
	// 				},
	// 				quantity: {
	// 					type: Number,
	// 					required: [true, "Quantity is required"]
	// 				}

	// 			}
	// 		],

	// 		totalAmount: {
	// 			type: Number,
	// 			required: [true, "Total Amount is required"]
	// 		},

	// 		purchasedOn: {
	// 			type: Date,
	// 			default: new Date()
	// 		}
	// 	}
	// ]


})






module.exports = mongoose.model("User", userSchema);