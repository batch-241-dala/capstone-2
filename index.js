const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoute = require('./routes/userRoute');
const productRoute = require('./routes/productRoute');
const orderRoute = require('./routes/orderRoute');
const port = 8080;
const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.me7rc81.mongodb.net/eCommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connected to MongoDB Atlas!"));



app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);



app.listen(port, () => {
	console.log(`API is now running on port ${port}`)
});